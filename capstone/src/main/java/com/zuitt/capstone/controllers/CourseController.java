package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;
    @RequestMapping(value = "/course",method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourse(),HttpStatus.OK);
    }
    @RequestMapping(value = "/course",method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestBody Course course){
        courseService.createCourse(course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }
}
