package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;

public interface CourseService {
void createCourse(Course course);
Iterable<Course> getCourse();
}
