package com.zuitt.capstone.services;

import com.zuitt.capstone.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}
